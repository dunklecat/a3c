# A3C implementation written in Python

## Requirements

* Python 3
* Tensorflow
* Numpy
* Skimage
* Scipy
* OpenAI Gym

## Goal

Working and documented A3C implementation capable of playing various games
