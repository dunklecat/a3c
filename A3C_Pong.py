# IMPORTS #####################################################################


import os

# LOCAL IMPORTS
# Global network implementation
from lib.global_network import run

###############################################################################

# MAIN ########################################################################

max_episode_length = 1000
gamma = .99     # discount rate for advantage estimation and reward discounting
s_size = 7056   # Observations are greyscale frames of 84 * 84 * 1
a_size = 3      # a_size = env.action_space.n
load_model = True

env_name = "Pong-v0"
model_path = './model_' + env_name
frames_path = './frames_' + env_name


if not os.path.exists(model_path):
    os.makedirs(model_path)

#Create a directory to save episode playback gifs to
if not os.path.exists(frames_path):
    os.makedirs(frames_path)

run(s_size, a_size, env_name, model_path, load_model, max_episode_length, gamma)


###############################################################################
