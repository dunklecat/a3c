# IMPORTS #####################################################################

# An open source machine learning framework for everyone
# https://www.tensorflow.org/
import tensorflow as tf
import tf.keras as keras
import keras.layers as layers

# NumPy is the fundamental package for scientific computing with Python.
# http://www.numpy.org/
import numpy as np

# LOCAL IMPORTS
from helper import normalized_columns_initializer

###############################################################################

# HELPER FUNCTION #############################################################

#Used to initialize weights for policy and value output layers
# def normalized_columns_initializer(std=1.0):
#     def _initializer(shape, dtype=None, partition_info=None):
#         out = np.random.randn(*shape).astype(np.float32)
#         out *= std / np.sqrt(np.square(out).sum(axis=0, keepdims=True))
#         return tf.constant(out)
#     return _initializer

###############################################################################

# CLASS #######################################################################


class AC_Network():
    'Class defining the structure of the network'

    def __call__(self,s_size,a_size,scope,trainer):
        network = AC_Network(self,s_size,a_size,scope,trainer)
        return network

    def __init__(self,s_size,a_size,scope,trainer):

        # with tf.variable_scope(scope):

        #Input
        input_layer = keras.Input(shape=[None,s_size],
                                  dtype='float32'
        )

        hidden_layer = layers.Reshape(target_shape=[-1,84,84,1])(input_layer)

        hidden_layer = layers.Conv2d(16, 8,
                                     activation = 'elu',
                                     strides = 4
        )(hidden_layer)

        # Second convolutional layer
        hidden_layer = layers.Conv2d(32, 4,
                                     activation = 'elu',
                                     stride = 2
        )(hidden_layer)


        hidden_layer = layers.Flatten()(hidden_layer)

        hidden_layer = layers.Dense(units = 256,
                                    activation = 'elu'
        )(hidden_layer)

        # Thinking about a Convlstm2d
        hidden_layer = layers.LSTM(256)(hidden_layer)

        # lstm_cell = tf.nn.rnn_cell.LSTMCell(256, state_is_tuple=True)
        # c_init = np.zeros((1, lstm_cell.state_size.c), np.float32)
        # h_init = np.zeros((1, lstm_cell.state_size.h), np.float32)
        # state_init = [c_init, h_init]
        # c_in = tf.placeholder(tf.float32, [1, lstm_cell.state_size.c])
        # h_in = tf.placeholder(tf.float32, [1, lstm_cell.state_size.h])
        # state_in = (c_in, h_in)
        # rnn_in = tf.expand_dims(hidden, [0])
        # step_size = tf.shape(self.imageIn)[:1]
        # state_in = tf.contrib.rnn.LSTMStateTuple(c_in, h_in)
        # lstm_outputs, lstm_state = tf.nn.dynamic_rnn(
        #     lstm_cell, rnn_in, initial_state=state_in, sequence_length=step_size,
        #     time_major=False
        # )
        # lstm_c, lstm_h = lstm_state
        # self.state_out = (lstm_c[:1, :], lstm_h[:1, :])
        # rnn_out = tf.reshape(lstm_outputs, [-1, 256])

        self.output_layer_policy = layers.Dense(a_size,
                                           activation = 'elu'
        )(hidden_layer)

        self.output_layer_value = layers.Dense(1,
                                          activation = 'elu'
        )(hidden_layer)

        # Output layers for policy and value estimations
        # self.policy = slim.fully_connected(rnn_out,a_size,
        #                                    activation_fn=tf.nn.softmax,
        #                                    weights_initializer=normalized_columns_initializer(0.01),
        #                                    biases_initializer=None
        # )
        # self.value = slim.fully_connected(rnn_out,1,
        #                                   activation_fn=None,
        #                                   weights_initializer=normalized_columns_initializer(1.0),
        #                                   biases_initializer=None
        # )

        self.model = keras.Model(inputs=[input_layer],
                                 outputs=[self.output_layer_policy, self.output_layer_value]
        )

        # From TF1 to TF2 arrived here

        # TODO Andare da worker.py e trasformare secondo le indicazioni presenti ciò che è qui sotto in funzioni
        #Only the worker network need ops for loss functions and gradient updating.
        # if scope != 'global':
        # self.actions = tf.placeholder(shape=[None],dtype=tf.int32)
        # self.actions_onehot = tf.one_hot(self.actions,a_size,dtype=tf.float32)
        # self.target_v = tf.placeholder(shape=[None],dtype=tf.float32)
        # self.advantages = tf.placeholder(shape=[None],dtype=tf.float32)

        # self.responsible_outputs = tf.reduce_sum(self.policy * self.actions_onehot, [1])

        #Loss functions
        # self.value_loss = tf.reduce_sum(tf.square(self.target_v - tf.reshape(self.value,[-1])))
        # self.entropy_loss = - tf.reduce_sum(self.policy * tf.log(self.policy))
        # self.entropy_loss = - tf.reduce_sum(self.policy * tf.log(self.policy + 10e-6))
        # self.policy_loss = -tf.reduce_sum(tf.log(self.responsible_outputs)*self.advantages)
        # self.loss = 0.5 * self.value_loss + self.policy_loss - self.entropy_loss * 0.01

        #Get gradients from local network using local losses
        # local_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope)
        # self.gradients = tf.gradients(self.loss,local_vars)
        # self.var_norms = tf.global_norm(local_vars)
        # grads,self.grad_norms = tf.clip_by_global_norm(self.gradients,40.0)

        #Apply local gradients to global network
        # global_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, 'global')
        # self.apply_grads = trainer.apply_gradients(zip(grads,global_vars))

    @tf.function
    def apply_gradients(self, target_v, actions, advantages, global_model):

        # Value_loss
        value_loss = tf.reduce_sum(tf.square(target_v - tf.reshape(self.output_layer_value, [-1])))

        # Policy_loss
        actions_onehot = tf.one_hot(actions, len(actions), ftype = tf.float32)
        responsible_outputs = tf.reduce_sum(self.output_layer_policy * actions_onehot, [1])
        policy_loss = - tf.reduce_sum(tf.log(responsible_outputs) * advantages)

        # Entropy_loss
        entropy_loss = - tf.reduce_sum(self.output_layer_policy * tf.log(self.output_layer_policy * 10e-6))

        # Grad_norms
        loss = 0.5 * value_loss + policy_loss - entropy_loss * 0.01
        local_vars = self.model.trainable_variables
        gradients = tf.gradients(loss, local_vars)
        grads, grad_norms = tf.clip_by_global_norm(gradients, 40.0)

        # Var_norms
        local_vars = model.trainable_variables
        var_norms = tf.global_norm(local_vars)

        # Apply_grads
        global_vars = global_model.trainable_variables
        apply_grads = trainer.apply_gradients(zip(grads, global_vars))

        return value_loss, policy_loss, entropy_loss, grad_norms, var_norms, apply_grads

    # @tf.function
    # def value_loss(self, target_v):
    #     self.value_loss = tf.reduce_sum(tf.square(target_v - tf.reshape(self.output_layer_value, [-1])))
    #     return self.value_loss

    # @tf.function
    # def policy_loss(self, actions, actions_size, advantages):
    #     actions_onehot = tf.one_hot(actions, actions_size, dtype = tf.float32)
    #     responsible_outputs = tf.reduce_sum(self.output_layer_policy * actions_onehot, [1])
    #     self.policy_loss = - tf.reduce_sum(tf.log(responsible_outputs) * advantages)
    #     return self.policy_loss

    # @tf.function
    # def entropy_loss(self):
    #     self.entrpy_loss = - tf.reduce_sum(self.output_layer_policy * tf.log(self.output_layer_policy * 10e-6))
    #     return self.entropy_loss

    # @tf.function
    # def grad_norms(self):
    #     loss = 0.5 * self.value_loss + self.policy_loss - self.entropy_loss * 0.01
    #     local_vars = self.model.trainable_variables
    #     gradients = tf.gradients(loss, local_vars)
    #     self.grads, self.grad_norms = tf.clip_by_global_norm(gradients, 40.0)
    #     return self.grad_norms

    # @tf.function
    # def var_norms(self):
    #     local_vars = self.model.trainable_variables
    #     self.var_norms = tf.global_norm(local_vars)
    #     return self.var_norms

    # @tf.function
    # def apply_grads(self, global_model):
    #     global_vars = self.global_model.trainable_variables 
    #     self.apply_grads = trainer.apply_gradients(zip(self.grads, global_vars))
    #     return self.apply_grads

###############################################################################
