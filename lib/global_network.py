# IMPORTS #####################################################################

import threading
import multiprocessing
import numpy as np
import scipy.signal

import gym
from skimage.color import rgb2gray

from random import choice
from time import sleep
from time import time

# An open source machine learning framework for everyone
# https://www.tensorflow.org/
import tensorflow as tf

import gym

# LOCAL IMPORTS
# Net skeleton
from lib.ac_network import *
# Agent implementation
from lib.worker import *
# Helper functions
from lib.helper import *

###############################################################################

# RUN #########################################################################

def run(space_size, a_size, env_name, model_path, load_model, max_episode_length, gamma):

    tf.keras.backend.clear_session()

    with tf.device(device_name="/cpu:0"):

        global_episodes = tf.Variable(initial_value=0,
                                      trainable=False,
                                      name='global_episodes',
                                      dtype=tf.int32,
        )

        trainer = tf.keras.optimizers.RMSProp(learning_rate = 7e-4,
                                              rho = 0.99,
                                              epsilon = 0.1
        )

        global_model = AC_Network(input_size=space_size,
                                    output_size=a_size,
                                    scope='global',
                                    trainer=None
        ) # Generate global network

        workers = create_workers()

        def create_workers(number_of_workers=1):
            workers = []
            # Create worker classes
            for i in range(num_workers):
                game = gym.make(env_name)
                workers.append(Worker(environment = game,
                                      number = i,
                                      input_size = space_size,
                                      action_size = action_size,
                                      trainer = trainer,
                                      model_path = model_path,
                                      global_episodes = global_episodes,
                                      global_model = global_model
                ))
            return workers

        checkpoint = tf.train.Checkpoint(max_to_keep=5)

    # with tf.Session() as sess:
    coord = tf.train.Coordinator()
    if load_model == True:
        print ('Loading Model...')
        ckpt = tf.train.get_checkpoint_state(model_path)
        try:
            checkpoint.restore(tf.train.latest_checkpoint(model_path))
        except Exception:
            print ("Model not found")
            # sess.run(tf.global_variables_initializer())
            tf.global_variables_initializer()
        else:
            # sess.run(tf.global_variables_initializer())
            tf.global_variables_initializer()


    # Start the "work" process for each worker in a separate thread.
    worker_threads = []
    for worker in workers:
        worker_work = lambda: worker.work(max_episode_length,gamma,sess,coord,checkpoint)
        t = threading.Thread(target=(worker_work))
        t.start()
        sleep(0.5)
        worker_threads.append(t)

    coord.join(worker_threads)

    # from tf to tf2: Arrived here

###############################################################################
