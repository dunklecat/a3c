# IMPORTS #####################################################################

# An open source machine learning framework for everyone
# https://www.tensorflow.org/
import tensorflow as tf
# import tensorflow.contrib.slim as slim

# From TF1 to TF2 Arrived here!

# NumPy is the fundamental package for scientific computing with Python.
# http://www.numpy.org/
import numpy as np

# scikit-image is a collection of algorithms for image processing.
# https://scikit-image.org/
from skimage.color import rgb2gray

import scipy

# LOCAL IMPORTS
from lib.helper import *
from lib.ac_network import *
###############################################################################

# HELPER FUNCTIONS ############################################################

# TODO I'll seee them at the end for TF1_2_TF2

# Copies one set of variables to another.
# Used to set worker network parameters to those of global network.
@tf.function
def update_target_graph(global_model, local_model):
    global_trainable_variables = global_model.trainable_variables
    local_trainable_variables  = local_model.trainable_variables

    op_holder = []
    for global_trainable_variable, local_trainable_variable in zip(global_trainable_variables, local_trainable_variables):
        op_holder.append(local_trainable_variable.assign(global_trainable_variable))
    return op_holder

    # TF1_2_TF2 arrived here

# Processes the screen image to produce cropped and resized image.
def process_frame(frame):
    new_frame = frame[10:-10, 30:-30]
    new_frame = rgb2gray(new_frame)
    new_frame = scipy.misc.imresize(new_frame, [84, 84])
    new_frame = np.reshape(new_frame, [np.prod(s.shape)]) / 255.0
    return new_frame

# Discounting function used to calculate discounted returns.
def discount(x, gamma):
    return scipy.signal.lfilter([1], [1, -gamma], x[::-1], axis=0)[::-1]


###############################################################################

# CLASS #######################################################################

class Worker():
    'Class defining a general agent'

    def __call__(self, environment, number, input_size, action_size, trainer, model_path, global_episodes, global_model):
        worker = Worker(self, environment, number, input_size, action_size, trainer, model_path, global_episodes, global_model)  #
        return worker

    def __init__(self, environment, number, input_size, action_size, trainer, model_path, global_episodes, global_model):
        self.name = "worker_" + str(number)
        self.model_path = model_path
        self.trainer = trainer
        self.global_episodes = global_episodes
        self.increment = self.global_episodes.assign_add(1)
        self.episode_rewards = []
        self.episode_lengths = []
        self.episode_mean_values = []
        self.summary_writer = tf.summary.FileWriter("train_"+str(self.name))

        #Create the local copy of the network and the tensorflow op to copy global paramters to local network
        self.local_model = AC_Network(input_size, action_size, self.name, trainer)
        # self.update_local_ops = update_target_graph(global_model, local_model)

        self.environment = environment
        self.actions = [1, 2, 3]

    # TODO
    def train(self, rollout, gamma, bootstrap_value):
        rollout = np.array(rollout)
        observations = rollout[:, 0]
        actions = rollout[:, 1]
        rewards = rollout[:, 2]
        next_observations = rollout[:, 3]
        values = rollout[:, 5]

        # Here we take the rewards and values from the rollout, and use them to
        # generate the advantage and discounted returns.
        # The advantage function uses "Generalized Advantage Estimation"
        self.rewards_plus = np.asarray(rewards.tolist() + [bootstrap_value])
        discounted_rewards = discount(self.rewards_plus, gamma)[:-1]
        self.value_plus = np.asarray(values.tolist() + [bootstrap_value])
        advantages = rewards + gamma * self.value_plus[1:] - self.value_plus[:-1]
        advantages = discount(advantages, gamma)

        # Update the global network using gradients from loss
        # Generate network statistics to periodically save
        # feed_dict = {self.local_model.target_v:discounted_rewards,
        #     self.local_model.inputs:np.vstack(observations),
        #     self.local_model.actions:actions,
        #     self.local_model.advantages:advantages,
        #     self.local_model.state_in[0]:self.batch_rnn_state[0],
        #     self.local_model.state_in[1]:self.batch_rnn_state[1]}

        # v_l, p_l, e_l, g_n, v_n, self.batch_rnn_state, _ = sess.run([self.local_model.value_loss,
        #     self.local_model.policy_loss,
        #     self.local_model.entropy_loss,
        #     self.local_model.grad_norms,
        #     self.local_model.var_norms,
        #     self.local_model.state_out,
        #     self.local_model.apply_grads],
        #     feed_dict=feed_dict)

        # TODO Far diventare ciò che è qui dentro e richiamato nel modello una @tf.function per se invece che solo delle variabili
        tmp = self.local_model.apply_gradients(discounted_rewards, actions, advantages, global_model)
        value_loss, policy_loss, entropy_loss, grad_norms, var_norms, _ = tmp

        value_loss = value_loss / len(rollout)

        # policy_loss = self.local_model.policy_loss(actions, len(actions), advantages)
        policy_loss = policy_loss / len(rollout)

        # entropy_loss = self.local_model.entropy_loss()
        entropy_loss = entropy_loss / len(rollout)

        # grad_norms = self.local_model.grad_norms()

        # var_norms = self.local_model.var_norms()

        # _ = self.local_model.apply_grads(global_model)

        return value_loss, policy_loss, entropy_loss, grad_norms, var_norms


    # def work(self, max_episode_length, gamma, sess, coord, saver):
    def work(self, max_episode_length, gamma, coord, saver):
        # episode_count = sess.run(self.global_episodes)
        episode_count = self.global_episodes.eval()

        total_steps = 0
        print ("Starting worker " + self.name)


        # with sess.as_default(), sess.graph.as_default():
        while not coord.should_stop():
            sess.run(self.update_local_ops) # TODO Seeing if later in the code there's something useful
            episode_buffer = []
            episode_values = []
            episode_frames = []
            episode_reward = 0
            episode_step_count = 0
            done = False


        state = self.environment.reset()
        episode_frames.append(state)
        # TODO Use the native gym wrapper to get already processed frames
        state = process_frame(state)
        # rnn_state = self.local_model.state_init
        # self.batch_rnn_state = rnn_state
        while done == False:
            # if self.name == 0:
            # self.environment.render()
            #Take an action using probabilities from policy network output.
            # a_dist, v, rnn_state = sess.run([self.local_model.policy,
            #                                  self.local_model.value,
            #                                  self.local_model.state_out],
            #                                 feed_dict={self.local_model.inputs:[state],
            #                                            self.local_model.state_in[0]:rnn_state[0],
            #                                            self.local_model.state_in[1]:rnn_state[1]}
            # )
            actions_distribution, value = self.local_model([state])

            action = np.random.choice(actions_distribution[0], p = actions_distribution[0])
            action = np.argmax(actions_distribution == action)


            new_state, reward, done, _ = self.environment.step(self.actions[a])

            if done == False:
                episode_frames.append(new_state)
                # TODO Use the gym wrapper!
                new_state = process_frame(new_state)
            else:
                new_state = state

            episode_buffer.append([state, action, reward, new_state, done, value[0, 0]])
            episode_values.append(value[0, 0])

            episode_reward += reward
            state = new_state
            total_steps += 1
            episode_step_count += 1


            # If the episode hasn't ended, but the experience buffer is full, then we
            # make an update step using that experience rollout.
            # if len(episode_buffer) == 30 and done != True and episode_step_count != max_episode_length - 1:
            if len(episode_buffer) == 50 and done != True and episode_step_count != max_episode_length - 1:
   			    # Since we don't know what the true final return is, we "bootstrap" from our current
                # value estimation.
                # v1 = sess.run(self.local_model.value,
                #               feed_dict={self.local_model.inputs:[state],
                #                          self.local_model.state_in[0]:rnn_state[0],
                #                          self.local_model.state_in[1]:rnn_state[1]
                #               }
                # )[0, 0]
                _, new_value = self.local_model([state])

                value_loss, policy_loss, entropy_loss, grad_norms, var_norms = self.train(episode_buffer,
                                                                                          gamma,
                                                                                          v1
                )
                episode_buffer = []
                # sess.run(self.update_local_ops)
                update_target_graph(global_model, local_model)

            if done == True:
                break

        self.episode_rewards.append(episode_reward)
        self.episode_lengths.append(episode_step_count)
        self.episode_mean_values.append(np.mean(episode_values))

        # work: TF1_2_TF2 arrived here!

        # Update the network using the episode buffer at the end of the episode.
        if len(episode_buffer) != 0:
            value_loss, policy_loss, entropy_loss, grad_norms, var_norms = self.train(episode_buffer, sess, gamma, 0.0)

        # Periodically save gifs of episodes, model parameters, and summary statistics.
        if episode_count % 5 == 0 and episode_count != 0:

            if self.name == 'worker_0' and episode_count % 25 == 0:
                time_per_step = 0.05
                images = np.array(episode_frames)
                make_gif(images, './frames/image'+str(episode_count)+'.gif',
                         duration=len(images)*time_per_step, true_image=True, salience=False)

            if self.name == 'worker_0' and episode_count % 50 == 0:
                print("Saving the model...")
                saver.save(sess, self.model_path+'/model-'+str(episode_count)+'.cptk')
                print ("Saved!")

            mean_reward = np.mean(self.episode_rewards[-5:])
            mean_length = np.mean(self.episode_lengths[-5:])
            mean_value = np.mean(self.episode_mean_values[-5:])
            summary = tf.Summary()
            summary.value.add(tag='Perf/Reward', simple_value=float(mean_reward))
            summary.value.add(tag='Perf/Length', simple_value=float(mean_length))
            summary.value.add(tag='Perf/Value', simple_value=float(mean_value))
            summary.value.add(tag='Losses/Value Loss', simple_value=float(v_l))
            summary.value.add(tag='Losses/Policy Loss', simple_value=float(p_l))
            summary.value.add(tag='Losses/Entropy', simple_value=float(e_l))
            summary.value.add(tag='Losses/Grad Norm', simple_value=float(g_n))
            summary.value.add(tag='Losses/Var Norm', simple_value=float(v_n))
            self.summary_writer.add_summary(summary, episode_count)
            self.summary_writer.flush()

        if self.name == 'worker_0':
            sess.run(self.increment)
        episode_count += 1

###############################################################################
